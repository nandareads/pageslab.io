# GitLab Pages examples

Request a new Pages example in the issue tracker.

If you want to submit a proposal for a new Pages site, read [CONTRIBUTING.md](CONTRIBUTING.md).

>**Note:** This website's source code is now hosted under https://gitlab.com/gitlab-com/www-gitlab-com.